# extract the lines with “categoryid”, and save them in a new file

import pandas as pd

# in the file 'items.csv', items has been sorted and deleted the duplications
path="C:/Users/carol/Downloads/archive/files/s/final/items.csv"

data=pd.read_csv(path,encoding="utf-8",dtype=str)
# extract the row which has "categoryid" in the column 'property'
newdf = data[(data.property == "categoryid")]
#newdata = data.query('property == "categoryid" ')
# generate a new intermediate file which will be used by belongto.py
newdf.to_csv("C:/Users/carol/Downloads/archive/files/intermediate_file/belongto1.csv", index=False, encoding="utf-8")


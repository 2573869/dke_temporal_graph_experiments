# extract the data that the timestamp is greater than 1431226800000 (the minimum time in the DB for the product)
import pandas as pd
from transformation.outils import timeStamp

# delete the data before 10/05
path = "C:/Users/carol/downloads/archive/events.csv"
df = pd.read_csv(path)
#sort by column 'timestamp'
data=df.sort_values(by="timestamp" , ascending=True) #1431216000000
data.to_csv('C:/Users/carol/Downloads/archive/files/intermediate_event/event_time.csv', mode='a+', index=False)
path="C:/Users/carol/Downloads/archive/files/intermediate_event/event_time.csv"
df = pd.read_csv(path)
#obtain the index
index_event=df[(df.timestamp>=1431226800000)].index.tolist()[0]
with open(path, 'r', encoding='utf-8') as f:
    csv_file = f.readlines()
with open(path[:-4] + '_deleted.csv', 'w+') as f:
    f.write(csv_file[0])
    f.writelines(csv_file[index_event+1:len(csv_file)-1])
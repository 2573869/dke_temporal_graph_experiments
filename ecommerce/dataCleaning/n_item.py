# sort the data and delete the duplications
import pandas as pd
import time
import os
from dataCleaning.outils import sortRows


# sort by column 'itemid','property','timestamp'
# in the class 'final', there are 34 files which are divided by the file complete_item.csv
# complete_item.csv has all of data of items
path = 'C:/Users/carol/Downloads/archive/files/source_item/complete'
cols=['itemid','property','timestamp']
data=sortRows(path,cols)
# read the files in the class 'final'
files = os.listdir(path)
# the list will contain all of the file in the class
csv_list = []
# add all of csv into the list 'csv_list'
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list.append(path + '\\' + f)
    else:
        pass
# Traverse every csv file
for i in range(0, len(csv_list)):
    data = pd.read_csv(csv_list[i], encoding='utf-8')
    st = time.time() # record the start time
    lenDf = data.shape[0] # length of the file
    head = data.columns.tolist() # extract the header of file as header for the new file
    width = len(head)
    listNull = [[None for x in range(0, width)] for y in range(lenDf)]
    df_item = pd.DataFrame(listNull, columns=head)
    print('file ', i + 2, ': start')
    df_item.loc[0, head] = data.loc[0, head]
    j = 1
    for index, row in data.iterrows():
        if index == 0:
            continue
        if data.at[index, 'itemid'] != data.at[index - 1, 'itemid'] or data.at[index, 'property'] != data.at[
            index - 1, 'property'] \
                or data.at[index, 'value'] != data.at[index - 1, 'value']:
            df_item.loc[j, head] = data.loc[index, head]
            j += 1
    # Intercept the part with data
    df_item = df_item[0:j]
    print(df_item.shape)
    df_item = df_item[(df_item.property != "categoryid")]
    et = time.time() # the end time
    cost_time = et - st
    print('file ', i + 2, 'cost time：{}seconds'.format(float('%.2f' % cost_time)))
    # save the file into the class /complete/f
    newPath = "C:/Users/carol/Downloads/archive/files/source_item/complete/f"
    df_item.to_csv(newPath + '\\' + csv_list[i][-11:] , index=False, encoding='utf-8')

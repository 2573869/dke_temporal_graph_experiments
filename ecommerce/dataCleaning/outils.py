import pandas as pd
import os
import time
def mergeFiles (path,fileName):
    """
    merge the files(.csv) in a class to one file(csv)
    :param path: the path of the class of the files to be merged
    :type path: str
    :param cols: the index of column to be saved (start with 0)
    :type path: list
    :param fileName: the name of the file after merge
    :type path: str
    :return: no return, generate a file csv in the same path
    """
    files = os.listdir(path)
    csv_list = []
    for f in files:
        if os.path.splitext(f)[1] == '.csv':
            csv_list.append(path + '\\' + f)
        else:
            pass
    df = pd.read_csv(csv_list[0],encoding='utf-8', low_memory=False)
    for i in range(1, len(csv_list)):
        df_i = pd.read_csv(csv_list[i], low_memory=False)
        pieces = [df[:], df_i[:]]
        df = pd.concat(pieces).drop_duplicates()
    df = df.iloc[:,: ]
    df.to_csv(path +'\\'+fileName+'.csv', index=None, encoding='utf-8')

def sortRows (path,cols):
    """
    sort the file by column given
    :param path: the path of the file to be sorted
    :type path: str
    :param cols: the columns referenced to sort the file
    :type cols: list
    :return: a dataframe well sorted
    :rtype:dataframe
    """
    data = pd.read_csv(path, encoding='utf-8')
    # sort by column 'itemid','property','timestamp'
    data.sort_values(by=cols, axis=0, ascending=[True, True, True], inplace=True)
    # reset the index of each row
    data.reset_index(drop=True, inplace=True)
    #data.to_csv(path[:-4]+'_sorted.csv', index=False, encoding='utf-8')
    return data


if __name__ == "__main__":
    #path = 'C:/Users/carol/Downloads/archive/files/s/final/f/itemfinal/merge'
    #path="C:/Users/carol/Downloads/archive/files/intermediate_event/addtocart"
    #path = "C:/Users/carol/Downloads/archive/files/intermediate_event/transaction/merge"
    #path="C:/Users/carol/Downloads/archive/files/intermediate_belongto/belongto/merge"
    path = "C:/Users/carol/Downloads/archive/files/intermediate_belongto/belongto/merge/merge_merge"
    #path="C:/Users/carol/Downloads/archive/files/intermediate_event/view/viewfinal"
    #path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/pro/pf"
    #cols=[0, 1, 2,3]
    st=time.time()
    # #mergeFiles(path,cols,'complete_item')
    # cols=['itemid', 'property', 'timestamp']
    # path='C:/Users/carol/Downloads/archive/files/s/complete_item.csv'
    # sortRows(path,cols)
    mergeFiles(path, 'belongto')
    et=time.time()
    cost_time=et-st
    print('cost time：{}seconds'.format(float('%.2f' % cost_time)))

# generate the file csv for the node 'category'
import numpy as np
import csv
from outils import min_time,convertTime

# Read the data from file 'category_tree.csv'
path='C:/Users/carol/Downloads/archive/category_tree.csv'

# Open file
with open(path, encoding='utf-8') as f:
    # load file in the variable data
    data = np.loadtxt(path,dtype=str, delimiter=',')
    #print(data)
# Obtain the length of data
num=len(data)
#print(num,data[0][0])

# Write data in the new file csv
field_order = ["categoryid:ID(Category)", 'startvalidtime', 'endvalidtime', ':LABEL']
# !!!!!!!!!!!!!
min_time="2015-05-10T00:00:00.000"
# Create the title of the file csv
with open("./files/category.csv", 'w', encoding="utf-8", newline='') as csvfile:
    writer = csv.DictWriter(csvfile, field_order)
    # Write the header of file
    writer.writeheader()
    # Write the content into file
    for i in range(1, num):
        writer.writerow(dict(zip(field_order, [data[i][0], min_time, "","Category"])))
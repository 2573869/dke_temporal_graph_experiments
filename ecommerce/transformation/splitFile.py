# split files by the number of lines
import time
import chardet

#path = "C:/Users/carol/Downloads/archive/files/s/complete_item_sorted.csv"
path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Proximity_T_ts_extracted.csv"

f = open(path, 'rb')
lines = f.readline()
file_code = chardet.detect(lines)['encoding']
with open(path, 'r', encoding=file_code) as f:
    csv_file = f.readlines()
linesPerFile = 100000
filecount = 1
st = time.time()
for i in range(0, len(csv_file), linesPerFile):
    with open(path[:-4] + '_' + str(filecount) + '.csv', 'w+') as f: #with open(path[:-4] + '_' + str(filecount) + '.csv', 'w+') as f:
        if filecount > 1:
            f.write(csv_file[0])
        f.writelines(csv_file[i:i + linesPerFile])
        filecount += 1

et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))

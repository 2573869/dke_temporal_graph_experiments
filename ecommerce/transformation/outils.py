import time

import pandas as pd
import numpy as np


def timeStampDay(timeNum):
    """
     Convert the time stamp to a time in normal format -day
    :param timeNum: time stamp to be converted
    :type timeNum:int
    :return : time in normal format
    :rtype : str
    """
    timeStamp = float(timeNum / 1000)
    timeArray = time.localtime(timeStamp)
    otherStyleTime = time.strftime("%Y-%m-%d", timeArray)
    timeFinal = otherStyleTime + "T00:00:00.000"
    return timeFinal


def timeStampDayNoZero(timeNum):
    """
     Convert the time stamp to a time in normal format - without millisecond
    :param timeNum: time stamp to be converted
    :type timeNum:int
    :return : time in normal format
    :rtype : str
    """
    timeStamp = float(timeNum / 1000)
    timeArray = time.localtime(timeStamp)
    otherStyleTime = time.strftime("%Y-%m-%d", timeArray)
    timeFinal = otherStyleTime + "T00:00:00"
    return timeFinal

def toTimestampNoMS(date):
    timeArray = time.strptime(date, "%Y-%m-%dT%H:%M:%S")
    timestamp = time.mktime(timeArray)
    return timestamp

def toTimestamp(date):
    """
    convert humain time to timestamp
    :param date: humain time format:"%Y-%m-%dT%H:%M:%S.000"
    :return: timestamp with 13 digits
    """
    timeArray = time.strptime(date, "%Y-%m-%dT%H:%M:%S.000")
    timestamp = time.mktime(timeArray)
    return round(timestamp)*1000


def timeStamp(timeNum):
    """
     Convert the time stamp to a time in normal format -millisecond
    :param timeNum: time stamp to be converted
    :type timeNum:int
    :return : time in normal format
    :rtype : str
    """
    time_stamp = float(timeNum / 1000)
    time_array = time.localtime(time_stamp)
    other_style_time = time.strftime("%Y-%m-%dT%H:%M:%S", time_array)
    time_final = other_style_time + '.' + str(int(timeNum % 1000))
    return time_final


def convertTimeDay(path: "the path of file to be converted", fileName: "the name of the file to be generated",
                   index: 'the colomne index of timestamps') -> "generate a file csv":
    """
    Convert the time stamp in the target file to a time in normal format and generate a new file
    :param path:the path of teh target file
    :type path: str
    :param fileName:the name and the path of file to be generated
    :type fileName: str
    :param index:the column index of the column where the timestamp is located
    :type index: int
    :return: no return, generate a new file
    """
    np_data = np.loadtxt(path, dtype="<U24", delimiter=",")
    for i in range(1, len(np_data)):
        if np_data[i][index] != '':
            np_data[i][index] = timeStampDay(int(np_data[i][index]))
    np.savetxt(fileName + '.csv', np_data, delimiter=",", fmt="%s")



def convertTimeDayNoFile(df_data: "the dataframe of data to be converted",indexs: 'the list of colomne name of timestamps') -> "generate a dataframe":
    """
    Convert the time stamp in the target file to a time in normal format and generate a new file
    :param path:the path of teh target file
    :type path: dataframe
    :param index:the list of the column name where the timestamp is located
    :type index: list
    :return: dataframe after the sort
    """
    for i in indexs :
        for index,row in df_data.iterrows(): #row!!!!!
            if df_data.loc[index, i] is not None:
                df_data.loc[index, i] = timeStampDay(int(df_data.loc[index, i]))
    return df_data

def convertTimeNaN(df_data: "the dataframe of data to be converted",indexs: 'the list of colomne name of timestamps') -> "generate a dataframe":
    """
    Convert the time stamp in the target file to a time in normal format and generate a new file skip the nan
    :param path:the path of teh target file
    :type path: dataframe
    :param index:the list of the column name where the timestamp is located
    :type index: list
    :return: dataframe after the sort
    """
    for i in indexs :
        for index,row in df_data.iterrows(): #row!!!!!
            if(not np.isnan(row[i])):

                df_data.loc[index, i] = timeStamp(int(df_data.loc[index, i]))
    return df_data

def convertTime(path: "the path of file to be converted", fileName: "the name of the file to be generated",
                index: "the index of column of the timestamp") -> "generate a file csv":
    """
    Convert the time stamp in the target file to a time in normal format and generate a new file
    :param path:the path of teh target file
    :type path: str
    :param fileName:the name and the path of file to be generated
    :type fileName: str
    :param index:the index of column of the timestamp
    :type index: int
    :return: no return, generate a new file
    """
    np_data = np.loadtxt(path, dtype="<U24", delimiter=",")
    for i in range(1, len(np_data)):
        np_data[i][index] = timeStamp(int(np_data[i][index]))
    np.savetxt(fileName + '.csv', np_data, delimiter=",", fmt="%s")


def min_time(path):
    """
    Find the minimum time in a file
    :param path: the path of the file
    :type path: str
    :return: the minimum timestamp
    :rtype: int
    """
    data = pd.read_csv(path, encoding='utf-8')
    min_time = min(data['timestamp'].tolist())
    return min_time


def max_time(path):
    """
    Find the maximum time in a file
    :param path: the path of the file
    :type path: str
    :return: the minimum timestamp
    :rtype: int
    """
    data = pd.read_csv(path, encoding='utf-8')
    max_time = max(data['timestamp'].tolist())
    return max_time

    # print('min_time :',timeStamp(min_time),'max_time:',timeStamp(max_time))


if __name__ == "__main__":
    # path="C:/Users/carol/Downloads/archive/files/s/final/f/test.csv"
    # data=pd.read_csv(path,encoding='utf-8')
    # print(data)
    # print(convertTimeDayNoFile(data,["time1","time2"]))

    df = pd.DataFrame({'a': [1434562341000, 1434562341000, np.nan], 'b': [np.nan, 1434562341000, np.nan]})
    print(df)
    print(convertTimeDayNaN(df,["a","b"]))

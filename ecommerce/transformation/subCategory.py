# generate the file csv for the relation 'subCategory'
import pandas as pd
from pandas import DataFrame

path = 'C:/Users/carol/Downloads/archive/category_tree.csv'
data=pd.read_csv(path, encoding='utf-8',dtype='str')
categoryids=data['categoryid'].tolist()
print(type(categoryids[0]))
parentids=data['parentid'].tolist()
print(parentids)
print(len(categoryids),len(parentids))
min_time="2015-05-10T00:00:00.000"
startvalidtime = [min_time]*len(categoryids)  # the minimum time
endvalidtime = [None]*len(categoryids)
type_relation = ["Subcategory"]*len(categoryids)
subCategory = {':START_ID(Category)': categoryids,'categoryid':categoryids, ':END_ID(Category)': parentids,'parentid': parentids, 'startvalidtime': startvalidtime, 'endvalidtime': endvalidtime, ':TYPE': type_relation}
df_subCategory = DataFrame(subCategory)
df_subCategory.to_csv("subCategory.csv", index=False, encoding='utf-8')
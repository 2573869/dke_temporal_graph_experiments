# generate the file csv for the node 'user'
import pandas as pd
from pandas import DataFrame

path = "C:/Users/carol/Downloads/archive/events.csv"
data = pd.read_csv(path, encoding='utf-8')

# Extract the data in the column 'visitorid'
userids = data['visitorid'].tolist()
# Remove the duplicates
userids = list(set(userids))
# Initialize other columns in the file
min_time="2015-05-10T00:00:00.000"
startvalidtime = [min_time]*len(userids)  # the minimum time
endvalidtime = [None]*len(userids)
label = ["User"]*len(userids)

print(len(userids),len(startvalidtime),len(endvalidtime),len(label))
user = {'userid:ID(User)': userids, 'startvalidtime': startvalidtime, 'endvalidtime': endvalidtime, ':LABEL': label}
df_user = DataFrame(user)
df_user.to_csv("./files/user.csv", index=False, encoding='utf-8')

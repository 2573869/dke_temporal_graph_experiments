# generate the file csv for the relation 'view'
import time

import pandas as pd
from pandas import DataFrame
from outils import timeStampDayNoZero, toTimestampNoMS, convertTimeNaN
import numpy as np
import os
na=np.nan

path1 = "C:/Users/carol/Downloads/archive/files/intermediate_event/view"
path2 = "C:/Users/carol/Downloads/archive/files/source_item/complete/f/itemfinal/timestamp"

csv_list1 = []
csv_list2 = []

files = os.listdir(path1)
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list1.append(path1 + '\\' + f)
    else:
        pass
files = os.listdir(path2)
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list2.append(path2 + '\\' + f)
    else:
        pass

for file in range(0, len(csv_list1)):
    st=time.time()
    data = pd.read_csv(csv_list1[file], encoding='utf-8')
    print(csv_list1[file])
    dataitem = pd.read_csv(csv_list2[file], encoding='utf-8')
    print(csv_list2[file])
    for index, row in data.iterrows():
        itemid = row["itemid"]

        timestamp = row["timestamp"]

        dayEvent = timeStampDayNoZero(timestamp)

        timestamp1 = round(toTimestampNoMS(dayEvent)*1000)

        index_instance = dataitem[(dataitem.itemid == itemid) & (dataitem.startvalidtime <= timestamp1) &( (
                    dataitem.endvalidtime >= timestamp1)|(dataitem.endvalidtime==0))].index.tolist()
        if (len(index_instance)==0):
            data=data.drop(index=index)
            continue
        instanceid = dataitem.at[int(index_instance[0]), "instanceid:ID(Item)"]

        data.at[index,'itemid']=instanceid

    instanceids= data['itemid']
    userids = data['visitorid']
    startvalidtime = data['timestamp']
    endvalidtime = data['timestamp']
    type_relation = ['View'] * len(userids)
    view = {':START_ID(User)': userids, 'userid': userids, ':END_ID(Item)': instanceids, 'instanceitemid': instanceids,
            'startvalidtime': startvalidtime, 'endvalidtime': endvalidtime, ':TYPE': type_relation}
    df_view = DataFrame(view)
    df_view = convertTimeNaN(df_view, ['startvalidtime', 'endvalidtime'])
    df_view.to_csv("view"+str(file+1)+".csv", index=False, encoding='utf-8')
    et = time.time()
    cost_time = et - st
    print('file ', file+1, ': cost time：{}seconds'.format(float('%.2f' % cost_time)))

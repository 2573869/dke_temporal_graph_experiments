# generate the file csv for the node 'item' with timestamp
import pandas as pd
import numpy as np
import time
import os
from outils import convertTimeDayNoFile, toTimestamp

path = "C:/Users/carol/Downloads/archive/files/source_item/complete/f/itemfinal/merge"
files = os.listdir(path)
csv_list = []
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list.append(path + '\\' + f)
    else:
        pass
ligne = 0
for file in range(0, len(csv_list)):
    st = time.time()
    print("start ", file, " file")

    data = pd.read_csv(csv_list[file], encoding="utf-8",dtype=str)
    for index, row in data.iterrows():
        row[ "startvalidtime"] = toTimestamp(row[ "startvalidtime"])
        if(type(row["endvalidtime"])!=float):
            row["endvalidtime"] = toTimestamp(row["endvalidtime"])
    data["startvalidtime"].fillna(0,inplace=True)
    data["endvalidtime"].fillna(0,inplace=True)
    data.to_csv(csv_list[file][:-4] + '_ts.csv', index=False, encoding='utf-8')
    et = time.time()
    cost_time = et - st
    print('file ', file + 1, ': cost time：{}seconds'.format(float('%.2f' % cost_time)))

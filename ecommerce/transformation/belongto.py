# generate the file csv for the relation 'belongto'
import time

import pandas as pd
from pandas import DataFrame
from outils import timeStampDayNoZero, toTimestampNoMS, convertTimeNaN
import numpy as np
import os
na=np.nan

path1 = "C:/Users/carol/Downloads/archive/files/intermediate_belongto/belongto"
path2 = "C:/Users/carol/Downloads/archive/files/source_item/complete/f/itemfinal/timestamp"

csv_list1 = []
csv_list2 = []

files = os.listdir(path1)
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list1.append(path1 + '\\' + f)
    else:
        pass
files = os.listdir(path2)
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list2.append(path2 + '\\' + f)
    else:
        pass

for file in range(0, len(csv_list1)):
    st=time.time()
    data = pd.read_csv(csv_list1[file], encoding='utf-8')
    print(csv_list1[file])
    dataitem = pd.read_csv(csv_list2[file], encoding='utf-8')
    print(csv_list2[file])
    # add startvalidtime and endvalidtime
    startvalidtime=data['timestamp']
    endvalidtime=data['timestamp']
    validtime={'startvalidtime':startvalidtime,'endvalidtime':endvalidtime}
    df_intermediate=pd.DataFrame(validtime)
    data.drop(axis=1, columns=['timestamp'], inplace=True)
    data= pd.concat([data, df_intermediate], axis=1)
    for index, row in data.iterrows():
        itemid = row["itemid"]

        timestamp = row["startvalidtime"]

        dayEvent = timeStampDayNoZero(timestamp)

        timestamp1 = round(toTimestampNoMS(dayEvent)*1000)

        # index_instance = dataitem[(dataitem.itemid == itemid) & (dataitem.startvalidtime <= timestamp1) &( (
        #             dataitem.endvalidtime >= timestamp1)|(dataitem.endvalidtime==0))].index.tolist()
        index_instance = dataitem[(dataitem.itemid == itemid) &( (
                    dataitem.endvalidtime >= timestamp1)|(dataitem.endvalidtime==0))].index.tolist()

        if (len(index_instance)==0):
            data=data.drop(index=index)
            continue
        # replace the itemid by instanceid
        instanceid = dataitem.at[int(index_instance[0]), "instanceid:ID(Item)"]
        data.at[index, 'itemid'] = instanceid
        data.at[index, 'startvalidtime'] = dataitem.at[int(index_instance[0]), "startvalidtime"]
        if dataitem.at[int(index_instance[0]), "endvalidtime"] != 0:
            data.at[index, 'endvalidtime'] = dataitem.at[int(index_instance[0]), "endvalidtime"]
        else:
            data.at[index, 'endvalidtime'] = None
        if len(index_instance) > 1:
            for y in range(1, len(index_instance)):
                # copy one row
                df_new = data.loc[[index]]
                indexs = data.index.tolist()
                newId = indexs[-1] + 1
                df_new.index = pd.Series(newId)
                # add the copy in the end of the dataframe
                data = pd.concat([data, df_new], axis=0, ignore_index=False)

                # change the itemid
                instanceid = dataitem.at[int(index_instance[y]), "instanceid:ID(Item)"]
                data.at[newId,'itemid']=instanceid

                # change the timestamp
                data.at[newId, 'startvalidtime'] = dataitem.at[int(index_instance[y]), "startvalidtime"]
                if dataitem.at[int(index_instance[y]), "endvalidtime"]!=0:
                    data.at[newId, 'endvalidtime'] = dataitem.at[int(index_instance[y]), "endvalidtime"]
                else:
                    data.at[newId, 'endvalidtime'] = None






    # sort by item time
    cols=['itemid','startvalidtime','endvalidtime']
    data.sort_values(by=cols, axis=0, ascending=[True, True,True], inplace=True)
    # reset the index of each row
    data.reset_index(drop=True, inplace=True)

    # extract the column and rebuild the dataframe
    instanceids= data['itemid']
    categoryids = data['value']
    startvalidtime = data['startvalidtime']
    endvalidtime = data['endvalidtime'] #[None] * len(categoryids)
    type_relation = ['Belongto'] * len(categoryids)
    belongto = {':START_ID(Item)': instanceids,'instanceitemid': instanceids,  ':END_ID(Category)': categoryids,'categoryid': categoryids,
                 'startvalidtime': startvalidtime, 'endvalidtime': endvalidtime,
                ':TYPE': type_relation}
    df_belongto = DataFrame(belongto)

    df_belongto = convertTimeNaN(df_belongto, ['startvalidtime'])
    df_belongto = convertTimeNaN(df_belongto, ['endvalidtime'])
    # generate new file belongto
    df_belongto.to_csv("belongto"+str(file+1)+".csv", index=False, encoding='utf-8')
    et = time.time()
    cost_time = et - st
    print('file ', file+1, ': cost time：{}seconds'.format(float('%.2f' % cost_time)))

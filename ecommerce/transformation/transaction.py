# generate the file csv for the relation 'transaction'
import pandas as pd
from pandas import DataFrame
from outils import timeStampDayNoZero, toTimestampNoMS, convertTimeNaN
import numpy as np
import os
na=np.nan
pd.set_option('display.float_format',lambda x: '%.0f'%x)

path1 = "C:/Users/carol/Downloads/archive/files/intermediate_event/transaction"
path2 = "C:/Users/carol/Downloads/archive/files/source_item/complete/f/itemfinal/timestamp"

csv_list1 = []
csv_list2 = []

files = os.listdir(path1)
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list1.append(path1 + '\\' + f)
    else:
        pass
files = os.listdir(path2)
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list2.append(path2 + '\\' + f)
    else:
        pass
for file in range(0, len(csv_list1)):
    data = pd.read_csv(csv_list1[file], encoding='utf-8')
    print(data)
    dataitem = pd.read_csv(csv_list2[file], encoding='utf-8')
    for index, row in data.iterrows():
        itemid = row["itemid"]

        timestamp = row["timestamp"]

        dayEvent = timeStampDayNoZero(timestamp)

        timestamp1 = round(toTimestampNoMS(dayEvent)*1000)

        index_instance = dataitem[(dataitem.itemid == itemid) & (dataitem.startvalidtime <= timestamp1) &( (
                    dataitem.endvalidtime >= timestamp1)|(dataitem.endvalidtime==0))].index.tolist()
        if (len(index_instance)==0):
            data=data.drop(index=index)
            continue
        instanceid = dataitem.at[int(index_instance[0]), "instanceid:ID(Item)"]
        data.at[index,'itemid']=instanceid

    print(data)
    instanceids= data['itemid']
    userids = data['visitorid']
    startvalidtime = data['timestamp']
    endvalidtime = data['timestamp']
    transactionids = data['transactionid']

    type_relation = ['Transaction'] * len(userids)
    transaction = {':START_ID(User)': userids, 'userid': userids, ':END_ID(Item)': instanceids, 'instanceitemid': instanceids,
                   'startvalidtime': startvalidtime, 'endvalidtime': endvalidtime, 'transactionid': transactionids,
                   ':TYPE': type_relation}


    df_transaction = DataFrame(transaction)
    df_transaction['transactionid'] = df_transaction['transactionid'].astype(int)
    df_transaction = convertTimeNaN(df_transaction, ['startvalidtime', 'endvalidtime'])
    df_transaction.to_csv("transaction_"+str(file+1)+".csv", index=False, encoding='utf-8')

# generate the file csv for the relation 'belongto'
import time

import pandas as pd
from pandas import DataFrame
from outils import timeStampDayNoZero, toTimestampNoMS, convertTimeNaN
import numpy as np
import os
na=np.nan

path1 = "C:/Users/carol/Downloads/archive/files/intermediate_belongto/belongto/merge"
path2 = "C:/Users/carol/Downloads/archive/files/source_item/complete/f/itemfinal/ttt"

csv_list1 = []
csv_list2 = []

files = os.listdir(path1)
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list1.append(path1 + '\\' + f)
    else:
        pass
files = os.listdir(path2)
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list2.append(path2 + '\\' + f)
    else:
        pass

for file in range(0, len(csv_list1)):
    stime=time.time()
    data = pd.read_csv(csv_list1[file], encoding='utf-8')
    print(csv_list1[file])
    dataitem = pd.read_csv(csv_list2[file], encoding='utf-8')
    print(csv_list2[file])
    # add startvalidtime and endvalidtime
    for index,row in data.iterrows():
        if data.at[index,'startvalidtime']== data.at[index,'endvalidtime']:
            st = dataitem[(dataitem.instanceid == data.at[index,'instanceitemid'])].index.tolist()
            et = dataitem[(dataitem.instanceid == data.at[index,'instanceitemid'])].index.tolist()
            data.at[index, 'startvalidtime']=dataitem.at[st[0],'startvalidtime']
            data.at[index, 'endvalidtime']=dataitem.at[et[0],'endvalidtime']



    # generate new file belongto
    data.to_csv("belongto_final"+str(file+1)+".csv", index=False, encoding='utf-8')
    et = time.time()
    cost_time = et - stime
    print('file ', file+1, ': cost time：{}seconds'.format(float('%.2f' % cost_time)))

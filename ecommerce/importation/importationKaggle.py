# Pycharm IDE
# This is the program to import the dataset e-commerce from kaggle .


import os


# remove data in the import folder in Neo4j configuration files
# in our virtual machine, the path of the import folder is /var/lib/neo4j/import
# if you are not on the same environment than us, please verify the location of the import folder at https://neo4j.com/docs/operations-manual/current/configuration/file-locations/
passWord ='******' # enter the password of your user account to authorize a linux command system in the virtual machine
cmd = 'sudo rm /var/lib/neo4j/import/*.csv'

# os.system(cmd) execute the command as in the terminal
# enter the password automatically
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# import csv files from the folder containing the dataset to the import file of Neo4j
# in the first argument of cmd, specify the folder in which the dataset for snapshots are and add /*.csv
cmd = 'sudo cp /home/activus01/data/kaggle/*.csv /var/lib/neo4j/import'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# clean database dedicated for the dataset of kaggle
# here our database is called kaggle3
cmd = 'sudo rm /var/lib/neo4j/data/databases/kaggle.db/*'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# stop neo4j server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j stop'
os.system('echo %s|sudo -S %s' % (passWord, cmd))


# import data
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j-admin import --database=kaggle.db ' \
      '--nodes=/var/lib/neo4j/import/user.csv ' \
      '--nodes=/var/lib/neo4j/import/category.csv ' \
      '--nodes=/var/lib/neo4j/import/item1_5.csv ' \
      '--nodes=/var/lib/neo4j/import/item6_10.csv ' \
      '--nodes=/var/lib/neo4j/import/item11_15.csv ' \
      '--nodes=/var/lib/neo4j/import/item16_20.csv ' \
      '--nodes=/var/lib/neo4j/import/item21_25.csv ' \
      '--nodes=/var/lib/neo4j/import/item26_30.csv ' \
      '--nodes=/var/lib/neo4j/import/item31_34.csv ' \
      '--relationships=/var/lib/neo4j/import/addtocart.csv ' \
      '--relationships=/var/lib/neo4j/import/transaction.csv ' \
      '--relationships=/var/lib/neo4j/import/view.csv ' \
      '--relationships=/var/lib/neo4j/import/belongto.csv ' \
      '--relationships=/var/lib/neo4j/import/subCategory.csv ' \
      '--skip-bad-relationships --skip-duplicate-nodes'



os.system('echo %s|sudo -S %s' % (passWord, cmd))

# start server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j start'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

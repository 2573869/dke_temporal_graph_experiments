**Experimental evaluation in the article "Graph data temporal evolutions: from conceptual modelling to implementation" (DKE 2021)**

/!\ The personal information of this gitlab account is temporarily anonymous during the process review of the article in DKE 2021. It is not possible to contact this account until the end of the process review. /!\


***Abstract of the paper***

When making decisions, a decision-maker needs not only historical data to an-alyze the _what?_ but also complementary information to understand the _why?_. Our work is set in the context of graph data. The objective is to keep track of graph evolutions to enable some why analyses. To do so, we propose a new conceptual model of temporal graphs to manage the evolution of graph data. It has the advantage of being generic enough to represent multi-levels evolution of the graph: (i) the topology, (ii) the attribute set of nodes and edges and, (iii) the attribute value of nodes and edges. We define a set of translation rules to convert our conceptual model into the logical property graph. Based on the translation rules, we implement several temporal graphs according to benchmark and real-world datasets in the Neo4j data store. These implementations allow us to carry out a comprehensive study of the feasibility and usability (through business analyses), the efficiency (saving up to 99% query executiontimes comparing to classic approches) and the scalability of our solution.

Keywords:Data models, Temporal graphs, Query, Neo4j.

***Summary of the experimental evaluation***

We run two experiments with the two following objectives:
- to evaluate the efficiency of our proposed temporal graph model by comparing its storage and query performance to the snapshot-based temporal graph. 
- to evaluate the scalability of our proposed model by comparing its query performance on different data volumes. 

We used benchmark (TPC-DS) and real-world datasets (E-commerce, Social Experiment and Citibike) to run both experiments.


***Technical environment***

The hardware configuration is as follows: PowerEdge R630, 16 CPUs x Intel(R) Xeon(R) CPU E5-2630 v3 @ 2.40Ghz, 63.91 GB. One virtual machine is installed on this hardware. This virtual machine (LINUX) has 6GB in terms of RAM and 100GB in terms of disk size. On the virtual machine, we installed the database management system **Neo4j Server** (the version **4.1.3** and the edition **Community**) as well as the IDE **Pycharm** (version **2020.1.2**).

_Install your technical environment:_
1) Install Neo4j Server.
2) Install Pycharm.
3) Open Pycharm.
4) Clone this remote repository in Pycharm. It will create a pycharm project. 


***Clean and transform datasets***


_TPC-DS dataset_

Temporal evolutions exist in a reference benchmark available online, namely [TPC-DS benchmark](http://www.tpc.org/tpc_documents_current_versions/pdf/tpc-ds_v2.13.0.pdf). This benchmark is based on transaction data of a retail company. It allows us to find all the three types of evolution: (i) attribute value, (ii) attribute set and (iii) topology. We used the dataset from this benchmark to answer the objective of evaluating the efficiency of our model compared to the snapshot-based model. To do so, we transformed the generated dataset from the benchmark into three datasets having our temporal graph, a classic snapshots and an optimized snapshots representations. We provide details about the transformation of TPC-DS dataset at the following repository: https://gitlab.com/2573869/temporal_graph_modelling.

_E-commerce dataset_

The E-commerce dataset has been collected from a real-world ecommerce website by RetailRocket company. It is available on [Kaggle](https://www.kaggle.com/retailrocket/ecommerce-dataset?select=item_properties_part2.csv). It is about customers' activity on the website (views, add to cart and transactions), ranging from May 10th 2015 to September 18th 2015. The dataset includes changes over time: (i) on item characteristics with the addition of new attributes over time and the change in attribute value  but also (ii) on the interactions between customers and items like clicks, add to carts and transactions. We used this dataset to answer the objective of evaluating the scalability of our model. To do so, we transformed the E-commerce dataset into our temporal graph representation. 

We can find in the folder [ecommerce](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/ecommerce) : 

- the translation of the original dataset into a temporal graph schema representation in the file [Kaggle_documentation_data_cleaning_transformation.pdf](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/blob/master/ecommerce/Kaggle_documentation_data_cleaning_transformation.pdf);
- the processes to clean and transform the dataset into our temporal graph representation in the file [Kaggle_documentation_data_cleaning_transformation.pdf](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/blob/master/ecommerce/Kaggle_documentation_data_cleaning_transformation.pdf);
- the python programs used in the processes to clean the dataset (i.e. treat incorrect and null values) in the folder [Datacleaning](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/ecommerce/dataCleaning);
- the python programs used in the processes to transform the dataset into our temporal graph representation in the folder [transformation](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/ecommerce/transformation); 


_Social experiment dataset_

The Social experiment dataset has been collected from a social experiment on students from MIT who lived in dormitory from October 2008 to May 2009 . It is available online at [Reality Commons website](http://realitycommons.media.mit.edu/socialevolution.html). This experiment was designed to study the adoption of political opinions, diet, exercise, obesity, eating habits, epidemiological contagion, depression and stress, dorm political issues, interpersonal relationships, and privacy. It includes daily surveys about the flu symptoms of students: (1) runny nose, nasal congestion, and sneezing; (2) nausea, vomiting, and diarrhea; (3) frequent stress; (4) sadness and depression; and (5) fever. Moreover, it includes physical information about proximity, location, call and Wi-Fi access points logs of the students every six minutes. Finally, it includes information about relationships between students (close friends, socialize twice per week, political discussant, share facebook and twitter activities). This dataset includes changes over time: (i) on the value of the symptoms of students and (ii) on the interactions between students. We used this dataset to answer the objective of evaluating the scalability of our model. To do so, we transformed the Social experiment dataset into our temporal graph representation. 

We can find in the folder [socialexperiment](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/socialexperiment) : 

- the translation of the original dataset into a temporal graph schema representation in the file [Social_experiment_documentation_data_cleaning_transformation.pdf](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/blob/master/socialexperiment/Social_experiment_documentation_data_cleaning_transformation.pdf);
- the processes to clean and transform the dataset into our temporal graph representation in the file  [Social_experiment_documentation_data_cleaning_transformation.pdf](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/blob/master/socialexperiment/Social_experiment_documentation_data_cleaning_transformation.pdf);
- the python programs used in the processes to clean the dataset (i.e. treat incorrect and null values) in the folder [Datacleaning](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/socialexperiment/dataCleaning);
- the python programs used in the processes to transform the dataset into our temporal graph representation in the folder [transformation](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/socialexperiment/transformation); 


_Citibike dataset_

The Citibike dataset is provided by the company Citibike. The Citibike company collects data about their bicycle rentals since the year 2013 in New York City and makes them avalaible [online](https://www.citibikenyc.com/system-data). We only limited ourselves on data from the year 2020 to 2021 because this period already represented a large volume of data. This dataset includes bike stations and trips between these stations. We identified that the attribute set describing trips between stations has changed since May 2021. Moreover, the value of the attributes describing trips changes over time. We used this dataset to answer the objective of evaluating the scalability of our model. To do so, we transformed the Citibike dataset into our temporal graph representation. 

We can find in the folder [citibike](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/citibike) : 

- the translation of the original dataset into a temporal graph schema representation in the file [Citibike_documentation_data_cleaning_transformation.pdf](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/blob/master/citibike/Citibike_documentation_data_cleaning_transformation.pdf);
- the processes to clean and transform the dataset into our temporal graph representation in the file [Citibike_documentation_data_cleaning_transformation.pdf](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/blob/master/citibike/Citibike_documentation_data_cleaning_transformation.pdf);
- the python programs used in the processes to clean the dataset (i.e. treat incorrect and null values) in the folder [Datacleaning](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/citibike/dataCleaning);
- the python programs used in the processes to transform the dataset into our temporal graph representation in the folder [transformation](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/citibike/transformation); 


***Donwload transformed datasets***


The datasets we use in our experiment are available here: 
- details about TPC-DS datasets are availaible at the following repository: https://gitlab.com/2573869/temporal_graph_modelling.
- original dataset, intermediate dataset (used in the cleaning/transformation processes) and the final dataset of E-commerce at the following link: https://cloud.irit.fr/index.php/s/tYpJS5k0mlpSOfl  
- original dataset, intermediate dataset (used in the cleaning/transformation processes) and the final dataset of Social experiment at the following link: https://cloud.irit.fr/index.php/s/ZQkToXDkaQyJmmV  
- original dataset, intermediate dataset (used in the cleaning/transformation processes) and the final dataset of Citibike at the following link: https://cloud.irit.fr/index.php/s/npYoLYklNiihgV6 


Save the datasets in a local directory.

***Import datasets in Neo4j***

_TPC-DS dataset_

We gave details on the importation of the TPC-DS dataset in Neo4j at the following repository: https://gitlab.com/2573869/temporal_graph_modelling.


_E-commerce dataset_


We can find in the folder [importation](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/ecommerce/importation) from the folder [ecommerce](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/ecommerce) the python program to import the dataset into Neo4j ;


_Social experiment dataset_

We can find in the folder [importation](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/socialexperiment/importation) from the folder [socialexperiment](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/socialexperiment) the python program to import the dataset into Neo4j ;

_Citibike dataset_

We can find in the folder [importation](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/citibike/importation) from the folder [citibike](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/citibike) the python program to import the dataset into Neo4j ;


***Query datasets in Neo4j***



_TPC-DS dataset_

We gave details about the querying of the TPC-DS dataset in Neo4j at the following repository: https://gitlab.com/2573869/temporal_graph_modelling.

_E-commerce dataset_

We can find in the folder [ecommerce](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/ecommerce) : 
- the benchmark queries (as well as their business meaning) that we run on E-commerce dataset in the folder [benchmarkqueries](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/ecommerce/benchmarkqueries);
- the python program to query the dataset into Neo4j based on benchmark queries in the folder [query](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/blob/master/ecommerce/query).


_Social experiment dataset_


We can find in the folder [socialexperiment](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/socialexperiment) : 
- the benchmark queries (as well as their business meaning) that we run on Social experiment dataset in the folder [benchmarkqueries](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/socialexperiment/benchmarkqueries);
- the python program to query the dataset into Neo4j based on benchmark queries in the folder [query](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/socialexperiment/query).

_Citibike dataset_


We can find in the folder [citibike](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/citibike) : 
- the benchmark queries (as well as their business meaning) that we run on Citibike dataset in the folder [benchmarkqueries](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/citibike/benchmarkqueries);
- the python program to query the dataset into Neo4j based on benchmark queries in the folder [query](https://gitlab.com/2573869/dke_temporal_graph_experiments/-/tree/master/citibike/query).







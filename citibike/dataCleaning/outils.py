def sortByCol (data,cols):
    """
    sort the dataframe by column given
    :param path: the path of the file to be sorted
    :type path: str
    :param cols: the columns referenced to sort the file
    :type cols: list
    :return: a dataframe well sorted
    :rtype:dataframe
    """
    ascending=[True for i in range (0,len(cols))]
    # sort by column
    data.sort_values(by=cols, axis=0, ascending=ascending, inplace=True)
    # reset the index of each row
    data.reset_index(drop=True, inplace=True)
    return data
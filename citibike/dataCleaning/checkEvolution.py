import pandas as pd
import time
import os
import numpy as np

path = "C:/Users/carol/Downloads/cityBike/intermediate"
files = os.listdir(path)
csv_list = []
father_list = []
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list.append(path + '\\' + f)
    else:
        pass
ligne = 0
for file in range(0, len(csv_list)):
    st = time.time()
    data = pd.read_csv(csv_list[file], encoding='utf-8')
    temp_list=data["start_station_id"].unique().tolist()+data["end_station_id"].unique().tolist()
    temp_list=set(temp_list)
    father_list.append(temp_list)

for i in range(0,len(father_list)-1):
    print(np.array_equal(father_list[i],father_list[i+1]))
    print(set(father_list[i]) ^ set(father_list[i+1]))

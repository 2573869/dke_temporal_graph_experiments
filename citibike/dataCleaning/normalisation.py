# normalize the time, remove the last 0 in the time 2020-01-01 00:00:55.3900
# reneme the column
import pandas as pd
import time
import os

path = "C:/Users/carol/Downloads/cityBike"
files = os.listdir(path)
csv_list = []
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list.append(path + '\\' + f)
    else:
        pass
ligne = 0
for file in range(0, len(csv_list)):
    st = time.time()
    data = pd.read_csv(csv_list[file], encoding='utf-8')
    # normalization of time
    for index, row in data.iterrows():
        # data.at[index, 'starttime'] = data.at[index, 'starttime'][:-1]
        # data.at[index, 'starttime'] = data.at[index, 'starttime'].replace(' ', 'T')
        # data.at[index, 'stoptime'] = data.at[index, 'stoptime'][:-1]
        # data.at[index, 'stoptime'] = data.at[index, 'stoptime'].replace(' ', 'T')
        data.at[index, 'startvalidtime'] = data.at[index, 'startvalidtime']+".000"
        data.at[index, 'startvalidtime'] = data.at[index, 'startvalidtime'].replace(' ', 'T')
        data.at[index, 'endvalidtime'] = data.at[index, 'endvalidtime']+".000"
        data.at[index, 'endvalidtime'] = data.at[index, 'endvalidtime'].replace(' ', 'T')

    # rename
    # data.rename(
    #     columns={'starttime': 'startvalidtime', 'stoptime': 'endvalidtime', 'start station id': 'start_station_id',
    #              'start station name': 'start_station_name', 'start station latitude': 'start_station_latitude',
    #              'start station longitude': 'start_station_longitude', 'end station id': 'end_station_id',
    #              'end station name': 'end_station_name', 'end station latitude': 'end_station_latitude',
    #              'end station longitude': 'end_station_longitude', 'birth year': 'birth_year'}, inplace=True)

    # save file
    data.to_csv("C:/Users/carol/Downloads/cityBike/intermediate/" + csv_list[file][34:-22] + ".csv", index=False,
                encoding='utf-8')

    et = time.time()
    cost_time = et - st
    print('file ' + str(file + 1) + ' cost time：{}seconds'.format(float('%.2f' % cost_time)))

# Pycharm IDE
# This is the program to import the dataset citibike.


import os


# remove data in the import folder in Neo4j configuration files
# in our virtual machine, the path of the import folder is /var/lib/neo4j/import
# if you are not on the same environment than us, please verify the location of the import folder at https://neo4j.com/docs/operations-manual/current/configuration/file-locations/
passWord ='*******' # enter the password of your user account to authorize a linux command system in the virtual machine
cmd = 'sudo rm /var/lib/neo4j/import/*.csv'

# os.system(cmd) execute the command as in the terminal
# enter the password automatically
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# import csv files from the folder containing the dataset to the import file of Neo4j
# in the first argument of cmd, specify the folder in which the dataset for snapshots are and add /*.csv
cmd = 'sudo cp /home/activus01/data/citybike/*.csv /var/lib/neo4j/import'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# clean database dedicated for the dataset of citybike
# here our database is called citybike
cmd = 'sudo rm /var/lib/neo4j/data/databases/citybike.db/*'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# stop neo4j server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j stop'
os.system('echo %s|sudo -S %s' % (passWord, cmd))


# import data
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j-admin import --database=citybike.db ' \
      '--nodes=/var/lib/neo4j/import/station.csv ' \
      '--relationships=/var/lib/neo4j/import/202102_trip.csv ' \
      '--relationships=/var/lib/neo4j/import/2020_trip.csv ' \
      '--relationships=/var/lib/neo4j/import/202101_trip.csv ' \
      '--relationships=/var/lib/neo4j/import/202103_trip.csv ' \
      '--relationships=/var/lib/neo4j/import/202104_trip.csv ' \
      '--relationships=/var/lib/neo4j/import/202105_trip.csv ' \
      '--skip-bad-relationships --skip-duplicate-nodes'

os.system('echo %s|sudo -S %s' % (passWord, cmd))

# start server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j start'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

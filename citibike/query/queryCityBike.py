
import py2neo as pn
import pandas as pd
import time
import numpy



# count the time of running the program
startGeneral = time.time()

#before you have to manually change the active database with respect to the volume you want to query
#in neo4j.conf set dbms.memory.max_heap_size = 4G
# connect DB
graph = pn.Graph("bolt://localhost:7687", auth=("neo4j", "activus01"), timeout=None)

# read csv of query


# where we find the csv of query
path_0 = '/home/activus01/data/querycitybike.csv'


# where we create the csv of result --- modify the name of the csv file with respect to the volume queried result1GB, result2GB
#result4GB, result8GB, result16GB
path_1 = '/home/activus01/data/resultscitybike.csv'

dfQuery = pd.read_table(path_0, sep=",")
dfResult = pd.DataFrame(columns=["Query", "result"])
dfResult.to_csv(path_1, index=False, sep=",")

for index, row in dfQuery.iterrows():
    # get the query one by one
    query = row[1]
    print(row[0])
    print(query)
    # for each query we execute 10 times
    # initialization of list and number of runs
    i = 1
    time_list = []
    while i < 11:
        # every times we count the execution time
        # time.time() is in seconds https://www.geeksforgeeks.org/get-current-time-in-milliseconds-using-python/
        time_start = time.time()
        graph.run(query)
        time_end = time.time()
        time_run = time_end - time_start
        time_list.append(time_run)
        i = i + 1
        # clear the cache
        graph.node_cache.clear() #py2neo4 version 2000.0.0
        graph.relationship_cache.clear()
    # delete max and min time of list
    time_list.remove(max(time_list))
    time_list.remove(max(time_list))
    time_final = round(numpy.mean(time_list), 5) #get 5 numbers after the comma
    # add in result
    addline = pd.DataFrame(columns=dfResult.columns)
    addline = addline.append({"Query": row[0], "result": time_final}, ignore_index=True)
    addline.to_csv(path_1, index=False, sep=",", mode='a', header=False)
    #dfResult = dfResult.append({"Query": row[0], "result": time_final}, ignore_index=True)
# create result csv
#dfResult.to_csv(path_1, index=False, sep=",")
endGeneral = time.time()
print('time cost', endGeneral - startGeneral, 's')


# transformation for station.csv
import pandas as pd
import time
import os

path = "C:/Users/carol/Downloads/cityBike/intermediate"
files = os.listdir(path)
csv_list = []
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list.append(path + '\\' + f)
    else:
        pass
ligne = 0
for file in range(0, len(csv_list)):
    st = time.time()
    data = pd.read_csv(csv_list[file], encoding='utf-8')

    print(data.shape[0])
    # colonne station id
    ids = data["start_station_id"].tolist()
    ide = data["end_station_id"].tolist()
    id = ids + ide
    print('id',len(id))
    #id = sorted(set(id), key=id.index)
    #id.sort()
    # colonne station name
    names = data["start_station_name"].tolist()
    namee = data["end_station_name"].tolist()
    name = names + namee
    print('name',len(name))
    #name = sorted(set(name), key=name.index)
    #name.sort()
    # colonne station latitude
    latitudes = data["start_station_latitude"].tolist()
    latitudee = data["end_station_latitude"].tolist()
    latitude = latitudes + latitudee
    print('latitude',len(latitude))
    #latitude = sorted(set(latitude), key=latitude.index)
    #latitude.sort()
    # colonne station longitude
    longitudes = data["start_station_longitude"].tolist()
    longitudee = data["end_station_longitude"].tolist()
    longitude = longitudes + longitudee
    print('longitude',len(longitude))
    #longitude = sorted(set(longitude), key=longitude.index)
    #longitude.sort()

    length=len(id)
    # colonne valid time: temps minimal de la dataset
    startvalidtime = ["2020-01-01T00:00:55.390" for i in range(0, length)]
    endvalidtime = ["" for i in range(0, length)]
    # label
    label=["Station" for i in range(0,length)]
    # combine dataframe
    stationObj = {'stationid:ID(Station)': id, 'stationname': name, 'startvalidtime': startvalidtime,
                  'endvalidtime': endvalidtime, 'stationlatitude': latitude, 'stationlongitude': longitude,':LABEL':label}
    data=pd.DataFrame(stationObj)
    data.to_csv("C:/Users/carol/Downloads/cityBike/final/" + csv_list[file][50:-4] + "_station_dupli.csv", index=False,
                encoding='utf-8')

    # delete duplication
    #data.drop_duplicates(keep="first", inplace=True)
    data.drop_duplicates(subset=['stationid:ID(Station)'],keep="first", inplace=True)
    # save file
    data.to_csv("C:/Users/carol/Downloads/cityBike/final/"+csv_list[file][50:-4]+"_station.csv", index=False, encoding='utf-8')

    et = time.time()
    cost_time = et - st
    print('file ' + str(file + 1) + 'cost time：{}seconds'.format(float('%.2f' % cost_time)))

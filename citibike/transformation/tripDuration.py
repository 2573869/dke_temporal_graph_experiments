# add column tripDuration
import pandas as pd
import time
import os
from outils import toTimestamp
path = "C:/Users/carol/Downloads/cityBike/final/tripDuration"  # granularité  problem
files = os.listdir(path)
csv_list = []
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list.append(path + '\\' + f)
    else:
        pass
ligne = 0
for file in range(0, len(csv_list)):
    st = time.time()
    data = pd.read_csv(csv_list[file], encoding='utf-8')

    # add new column tripduration
    tripduration=[None for i in range(0,data.shape[0])]
    tripdurationObj={'tripduration':tripduration}
    df_tripduration=pd.DataFrame(tripdurationObj)
    data=pd.concat([df_tripduration, data], axis=1)
    # add duration for each trip
    for index, row in data.iterrows():
        data.at[index,'tripduration']=toTimestamp(data.at[index,'endvalidtime'][:-4])-toTimestamp(data.at[index,'startvalidtime'][:-4])

    # save the file
    data.to_csv("C:/Users/carol/Downloads/cityBike/final/tripDuration" + csv_list[file][52:-4] + "_tripD.csv",
                index=False, encoding='utf-8')

    et = time.time()
    cost_time = et - st
    print('file ' + str(file + 1) + ' cost time：{}seconds'.format(float('%.2f' % cost_time)))

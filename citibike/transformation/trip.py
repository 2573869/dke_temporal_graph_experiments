# transformation for trip.csv
import pandas as pd
import time
import os

path = "C:/Users/carol/Downloads/cityBike/intermediate"
files = os.listdir(path)
csv_list = []
for f in files:
    if os.path.splitext(f)[1] == '.csv':
        csv_list.append(path + '\\' + f)
    else:
        pass
ligne = 0
for file in range(0, len(csv_list)):
    st = time.time()
    data = pd.read_csv(csv_list[file], encoding='utf-8')
    # supprimer colonnes start station name, end station name, start station latitude,
    # start station longitude, end station latitude, end station longitude
    data.drop(['start_station_name', 'start_station_latitude','start_station_longitude','end_station_name','end_station_latitude','end_station_longitude'], axis=1,inplace=True)
    # rename for :START_ID(Station),:END_ID(Station)
    data.rename(columns={'start_station_id':':START_ID(Station)','end_station_id':':END_ID(Station)'},inplace=True)

    # :TYPE
    relationType=["Trip" for i in range(0,data.shape[0])]
    typeObj={':TYPE':relationType}
    df_type=pd.DataFrame(typeObj)
    data=pd.concat([data,df_type],axis=1)

    # save file
    data.to_csv("C:/Users/carol/Downloads/cityBike/final/"+csv_list[file][47:-4]+"_trip.csv", index=False, encoding='utf-8')

    et = time.time()
    cost_time = et - st
    print('file ' + str(file + 1) + ' cost time：{}seconds'.format(float('%.2f' % cost_time)))
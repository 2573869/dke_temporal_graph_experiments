import time
import pandas as pd
import os

def mergeFiles (path,fileName):
    """
    merge the files(.csv) in a class to one file(csv)
    :param path: the path of the class of the files to be merged
    :type path: str
    :param cols: the index of column to be saved (start with 0)
    :type path: list
    :param fileName: the name of the file after merge
    :type path: str
    :return: no return, generate a file csv in the same path
    """
    files = os.listdir(path)
    csv_list = []
    for f in files:
        if os.path.splitext(f)[1] == '.csv':
            csv_list.append(path + '\\' + f)
        else:
            pass
    df = pd.read_csv(csv_list[0],encoding='utf-8', low_memory=False)
    for i in range(1, len(csv_list)):
        df_i = pd.read_csv(csv_list[i], low_memory=False)
        pieces = [df[:], df_i[:]]
        df = pd.concat(pieces).drop_duplicates()
    df = df.iloc[:,: ]
    df.to_csv(path +'\\'+fileName+'.csv', index=None, encoding='utf-8')
def toTimestamp(date):
    timeArray = time.strptime(date, "%Y-%m-%dT%H:%M:%S")
    timestamp = round(time.mktime(timeArray))
    return timestamp

if __name__=='__main__':
    path="C:/Users/carol/Downloads/cityBike/final"
    mergeFiles(path,'trip2020')
    # path="C:/Users/carol/Downloads/cityBike/final/station/station.csv"
    # data=pd.read_csv(path,encoding='utf-8')
    # data.drop_duplicates(subset=['stationid:ID(Station)'],keep="first", inplace=True)
    # # save file
    # data.to_csv("C:/Users/carol/Downloads/cityBike/final/station.csv", index=False,encoding='utf-8')

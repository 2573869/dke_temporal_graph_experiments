# transformation of BlogLivejournalTwitter.csv
import pandas as pd
import time
from pandas import DataFrame
from outils import sortByCol,toTimestampNoHMS,convertTimeNoHMS

# show all of the column
#pd.set_option('display.max_columns', None)
# show all of the rows
pd.set_option('display.max_rows', None)
st=time.time()
path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/BlogLivejournalTwitter_ts_extracted.csv"
data=pd.read_csv(path,encoding='utf-8')

#path_student="C:/Users/carol/Downloads/SocialEvolution/socialEvolution/Student_ts1.csv"
path_student="C:/Users/carol/Downloads/SocialEvolution/socialEvolution/Student_ts.csv"

df_student=pd.read_csv(path_student,encoding='utf-8')

# sort the data by the users and time
data=sortByCol(data,['id_A','id_B','startvalidtime'])

# # add startvalidtime and endvalidetime
# data.rename(columns={'survey_date': 'startvalidtime'},inplace=True)
# endvalidtime=data['startvalidtime']
# endvalidtime.rename('endvalidtime',inplace=True)
# data = pd.concat([data, endvalidtime], axis=1)
# add relation type and instanceid of for id_A and id_B
relationtype=['BlogLivejournalTwitter' for i in range(0,data.shape[0])]
instancestudentidA=["" for i in range(0,data.shape[0])]
instancestudentidB=["" for i in range(0,data.shape[0])]
intermediate = {':START_ID(Student)':instancestudentidA,':END_ID(Student)':instancestudentidB,':TYPE':relationtype}
df_intermediate = DataFrame(intermediate)
df_intermediate.reset_index(drop=True, inplace=True)

data = pd.concat([data, df_intermediate], axis=1)

# replace id by instancestudentid with the right time
# ( it's possible that 2 instances of tha same user are in the same day)
print(data.shape)
for index,row in data.iterrows():

    idAs=df_student[(df_student.user_id==row['id_A'])&(((df_student.startvalidtime<=row['startvalidtime'])&(df_student.endvalidtime>=row['startvalidtime']))|((df_student.startvalidtime<=row['endvalidtime'])&(df_student.endvalidtime>=row['endvalidtime']))|((df_student.startvalidtime>=row['startvalidtime'])&(df_student.endvalidtime<=row['endvalidtime'])))].index.tolist()
    if len(idAs) ==0:
        data.drop(axis=0,index=index,inplace=True)
        # vue que il existe pas de symptome de la grippe pour les etudiants, du coup, on n'a pas besoin de garder les infos, si on veux etudier la propagation de la grippe
        continue
    idBs=df_student[(df_student.user_id==row['id_B'])&(((df_student.startvalidtime<=row['startvalidtime'])&(df_student.endvalidtime>=row['startvalidtime']))|((df_student.startvalidtime<=row['endvalidtime'])&(df_student.endvalidtime>=row['endvalidtime']))|((df_student.startvalidtime>=row['startvalidtime'])&(df_student.endvalidtime<=row['endvalidtime'])))].index.tolist()
    if len(idBs) == 0:
        data.drop(axis=0, index=index, inplace=True)
        continue

    if len(idAs)==1&len(idBs)==1:
        # intersection????
        idA = df_student.at[int(idAs[0]), 'instancestudentid:ID(Student)']
        idB = df_student.at[int(idBs[0]), 'instancestudentid:ID(Student)']
        data.at[index, ':START_ID(Student)'] = idA
        data.at[index,':END_ID(Student)']=idB
        #print("1",data.loc[[index]])


    if len(idAs)>=2|len(idBs)>=2:
        # intersection ??
        intersection=0
        for x in range(0, len(idAs)):
            for y in range(0, len(idBs)):
                if(df_student.at[idAs[x],'startvalidtime']<=df_student.at[idBs[y],'endvalidtime']|df_student.at[idAs[x],'endvalidtime']>=df_student.at[idBs[y],'startvalidtime']):
                    intersection=intersection+1
                    idA = df_student.at[int(idAs[x]), 'instancestudentid:ID(Student)'] # instanceid
                    idB = df_student.at[int(idBs[y]), 'instancestudentid:ID(Student)'] # instanceid

                    if(intersection==1):

                        data.at[index, ':START_ID(Student)'] = idA
                        data.at[index, ':END_ID(Student)'] = idB
                    #    print("inter1",data.loc[[index]])
                        continue
                    if (intersection>1):# 2nd intersection  copy last row, modify id
                        # copy the row
                      #  df_new = pd.DataFrame(data[index:int(index) + 1])
                        df_new = data.loc[[index]]
                        indexs = data.index.tolist()
                        newId=indexs[-1] + 1
                        df_new.index = pd.Series(newId)
                        # add the copy in the end of the dataframe
                        data = pd.concat([data, df_new], axis=0, ignore_index=False)
                        # change the user_id by instancestudentid
                        data.at[indexs[-1] + 1, ':START_ID(Student)'] = idA
                        data.at[indexs[-1] + 1, ':END_ID(Student)'] = idB

# remove empty item
data.dropna(axis=0, how='any', inplace=True)

# convert timestamp to human time
data=convertTimeNoHMS(data,['startvalidtime','endvalidtime'])

# save into a new csv file
data.to_csv(path[:-58]+"socialEvolution/BlogLivejournalTwitter1.csv", index=False, encoding='utf-8')
print("successful operation")
et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))
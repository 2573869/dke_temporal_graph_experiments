# transformation for Access.csv
import pandas as pd
import time
from outils import convertNeo4jTime, toTimestamp, toTimestampNoMS, convertTime,timeStamp
st=time.time()
path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Access_ts_extracted_12.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/acc.csv"

#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/test.csv"
data=pd.read_csv(path,encoding='utf-8')

path_student="C:/Users/carol/Downloads/SocialEvolution/socialEvolution/Student_ts.csv"
df_student=pd.read_csv(path_student,encoding='utf-8')

# remove column unix_time
data.drop(axis=1, columns=['unix_time'], inplace=True)

# add column instanceuser
instanceuserid=["" for i in range(0,data.shape[0])]
relationtype=['Access' for i in range(0,data.shape[0])]
instance={'START_ID':instanceuserid,':TYPE':relationtype}

df_instance=pd.DataFrame(instance)
data=pd.concat([df_instance,data],axis=1)
# add instanceid user
for index,row in data.iterrows():
    print(index)
    indexusers = df_student[
        (row['user_id'] == df_student.user_id) & (df_student.startvalidtime <= row['startvalidtime']) & (
                    df_student.endvalidtime + 86399 >= row['endvalidtime'])].index.tolist()
    if (len(indexusers) == 1 ):
        userid = indexusers[0]
        data.at[index, 'START_ID'] = userid
        # if data.loc[index, "startvalidtime"] != "":
            # data.loc[index, "startvalidtime"] = timeStamp(int(data.loc[index, "startvalidtime"]))
            # data.loc[index, "endvalidtime"]=data.loc[index, "startvalidtime"]
# remove the rows where the column valid time is empty
print("remove the rows where the column valid time is empty")
data=data[(data.START_ID!="")]
print("fin de remove")

# convert timestamp to human time
# print("convert time")
# data=convertTime(data,['startvalidtime','endvalidtime'])
# print("fin de convert")

# rename
#data.rename(columns={'START_ID': ':START_ID(Student)','END_ID':':END_ID(WLAN)'},inplace=True)
data.rename(columns={'START_ID': ':START_ID(Student)','wireless_mac':':END_ID(WLAN)'},inplace=True)

# save the file
data.to_csv(path[:-4]+"aaaaaa.csv", index=False, encoding='utf-8')
#data.to_csv(path[:-7]+"acc1.csv", index=False, encoding='utf-8')

#data.to_csv(path[:-27]+"socialEvolution/Access.csv", index=False, encoding='utf-8')

et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))
# extract the data from 01/09/2009 to 04/25/2009
import pandas as pd
import time

# show all of the column
# pd.set_option('display.max_columns', None)
# show all of the rows
# pd.set_option('display.max_rows', None)
# read the file
path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/FluSymptoms_ts2.csv"
# path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Proximity_T_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Access_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/calls_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/receiveSMS_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/sendSMS_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/WLAN_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/BlogLivejournalTwitter_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/CloseFriend_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/FacebookAllTaggedPhotos_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/PoliticalDiscussant_ts.csv"
#path = "C:/Users/carol/Downloads/SocialEvolution/intermediate_files/SocializeTwicePerWeek_ts.csv"


st=time.time()
data = pd.read_csv(path, encoding='utf-8')

#data = data[(data.startvalidtime >= 1231455600 )&( data.startvalidtime <= 1240610400)]
data = data[(data.time >= 1231455600 )&( data.time <= 1240610400)]

data.to_csv(path[:-4]+"_extracted.csv", index=False, encoding='utf-8')

et=time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))
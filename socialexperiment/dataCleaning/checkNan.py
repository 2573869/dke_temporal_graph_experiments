# check Nan
import pandas as pd
import time
from outils import sortByCol
st=time.time()
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/Proximity.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/Calls.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/calls.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/SMS.csv"
#path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/SMS.csv"
path="C:/Users/carol/Downloads/SocialEvolution/Access.csv"


data=pd.read_csv(path,encoding='utf-8',dtype=str)
# sort by user_id and remove user id and time
#data=sortByCol(data,['user_id','remote_user_id_if_known','time'])


# remove empty item
#data.dropna(axis=0,subset=['user_id','remote_user_id_if_known'], inplace=True)
#data.dropna(axis=0,subset=['user_id','dest_user_id_if_known'], inplace=True)
# remove the rows that contain empty value
data.dropna(axis=0,how='any', inplace=True)

#data.to_csv(path[:-7]+"intermediate_files/SMS.csv", index=False, encoding='utf-8')
#data.to_csv(path, index=False, encoding='utf-8')
data.to_csv(path[:-10]+"intermediate_files/Access.csv", index=False, encoding='utf-8')

et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))
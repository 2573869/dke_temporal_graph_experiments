# split SMS.csv by the number of incoming
import pandas as pd
import time
from outils import sortByCol

# show all of the column
#pd.set_option('display.max_columns', None)
# show all of the rows
pd.set_option('display.max_rows', None)
st=time.time()
path="C:/Users/carol/Downloads/SocialEvolution/intermediate_files/SMS.csv"
data=pd.read_csv(path,encoding='utf-8',dtype=str)

# sort by incoming
data=sortByCol(data,['incoming'])
print(data)
# find the last index of 0
indexZero=data[data.incoming=='0'].index.tolist()[-1]
# split file into 2 files
df_sendSMS=data[0:indexZero+1]
df_receiveSMS=data[indexZero+1:]

# save to csv
df_sendSMS.to_csv(path[:-7]+"sendSMS.csv", index=False, encoding='utf-8')
df_receiveSMS.to_csv(path[:-7]+"receiveSMS.csv", index=False, encoding='utf-8')
et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))
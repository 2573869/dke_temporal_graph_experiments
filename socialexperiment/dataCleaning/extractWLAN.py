# extract  wireless_mac into a file WLAN.csv
import pandas as pd
import time

st = time.time()

path = "C:/Users/carol/Downloads/SocialEvolution/WLAN2.csv"
data = pd.read_csv(path, encoding='utf-8',dtype=str)
df_wlan=data['wireless_mac']
df_wlan.to_csv(path[:-9]+"intermediate_files/WLAN.csv", index=False, encoding='utf-8')

et = time.time()
cost_time = et - st
print('cost time：{}seconds'.format(float('%.2f' % cost_time)))